#include "ofApp.h"

using namespace ofxCv;
using namespace cv;
#include <glm/gtx/matrix_decompose.hpp>

void ofApp::setup() {
	ofSetVerticalSync(true);
	
    glm::mat4 rigid;
	glm::vec3 translation(ofRandomf(), ofRandomf(), ofRandomf());
    glm::quat rotation(ofRandomf(), glm::vec3(ofRandomf(), ofRandomf(), ofRandomf()));
    rigid = glm::translate(rigid, translation);
	rigid = glm::rotate(rigid,ofRandomf(), glm::vec3(ofRandomf(), ofRandomf(), ofRandomf()));
	
	vector<glm::vec3> from;
	for(int i = 0; i < 4; i++) {
		from.push_back(glm::vec3(ofRandom(1,2), ofRandom(1,2), ofRandom(5,6)));
		from.push_back(glm::vec3(ofRandom(3,4), ofRandom(3,4), ofRandom(5,6)));
		from.push_back(glm::vec3(ofRandom(1,2), ofRandom(3,4), ofRandom(5,6)));
		from.push_back(glm::vec3(ofRandom(3,4), ofRandom(1,2), ofRandom(5,6)));
	}
	
	vector<glm::vec3> to;
	for(int i = 0; i < from.size(); i++) {
		// opencv assumes you're doing premultiplication
        glm::vec3 rigidPreMult = glm::vec3(glm::vec4(from[i],1) * rigid);
		to.push_back(rigidPreMult);
	}
	
    const glm::mat4 rigidEstimate = estimateAffine3D(from, to);

	cout << "original matrix: " << endl << rigid << endl;
	cout << "estimated as: " << endl << rigidEstimate << endl;
	
	for(int i = 0; i < from.size(); i++) {
		// opencv assumes you're doing premultiplication
		glm::vec3 after = glm::vec3(glm::vec4(from[i],1) * rigidEstimate);
		cout << from[i] << " -> " << to[i] << " estimated as: " << after << endl;
	}
	
	glm::vec3 decompTranslation, decompScale, decompSkew;
    glm::vec4 decompPerspective;
    glm::quat decompRotation;
    glm::decompose(rigidEstimate,
            decompScale,
            decompRotation,
            decompTranslation,
            decompSkew,
            decompPerspective);
	cout << "translation: " << translation << endl;
	cout << "estimated as: " << decompTranslation << endl;
	cout << "rotation: " << endl << rotation << endl;
	cout << "skew: " << endl << decompSkew << endl;
	cout << "estimated as: " << endl << decompRotation << endl;
}

void ofApp::update() {
}

void ofApp::draw() {
	ofBackground(0);
	ofDrawBitmapString("See console window for results.", 10, 20);
}
