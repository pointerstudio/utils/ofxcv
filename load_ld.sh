#!/bin/bash

CURRENT_DIR=$(pwd)
OFX_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
cd ../..
SCRIPT_OF_ROOT=$(pwd)
cd $CURRENT_DIR

export LD_LIBRARY_PATH=${OFX_DIR}/libs/opencv4/lib
