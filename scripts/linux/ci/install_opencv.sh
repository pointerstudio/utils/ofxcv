#/bin/bash

# License
#
# Copyright (c) 2020 Jakob Schloetter studio@pointer.click
# Distributed under the MIT License.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#


###########################
# install opencv on linux #
###########################

PROGRESS_COMMENT () {
    echo ""
    echo "###########################"
    echo "# install opencv on linux #"
    echo "###########################"
    echo "-> ${1}"
    echo ""
}


# orientation
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
OFX_DIR=${SCRIPT_DIR}/../../..

#beautify
cd ${OFX_DIR}
OFX_DIR=$(pwd)
cd ${SCRIPT_DIR}

LIBS_DIR=${OFX_DIR}/libs
INSTALL_DIR=${LIBS_DIR}/opencv4
DOCKER_DIR=${SCRIPT_DIR}/opencv_docker
SHARED_DIR=${DOCKER_DIR}/shared

# verification
pre_verification() {
    if [ ! -d "$DOCKER_DIR" ]; then
        echo "whoops, where is ${DOCKER_DIR}?"
        exit 1
    fi
    if [ ! -d "$SHARED_DIR" ]; then
        mkdir -p $SHARED_DIR
    fi
    if [ ! -x "$(command -v docker)" ]; then
        echo "please install docker, or manually install opencv 4.5.0 to ${INSTALL_DIR}"
        exit 1
    fi
    PROGRESS_COMMENT "pre_verification done"
}

pre_clean() {
    sudo rm -rf ${SHARED_DIR}/*
    PROGRESS_COMMENT "pre_clean done"
}

build_docker() {
    cd $DOCKER_DIR
    docker build --build-arg CMAKE_INSTALL_PREFIX=${INSTALL_DIR} . -t opencv450 
    PROGRESS_COMMENT "build_docker done"
}

compile_opencv() {
    docker run -v ${SHARED_DIR}:/shared -i -t opencv450 cp -r ${INSTALL_DIR} /shared/

    PROGRESS_COMMENT "compile_opencv done"
}

install_opencv() {
    cp -r ${SHARED_DIR}/opencv4 ${LIBS_DIR}/

    PROGRESS_COMMENT "install_opencv done"
}

pre_verification
pre_clean
build_docker
compile_opencv
install_opencv
